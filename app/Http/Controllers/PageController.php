<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Product;
use App\Slide;
use App\TypeProduct;
use App\User;
use Hash;
use Auth;
use App\Cart;
use App\Customer;
use App\Bill;
use App\BillDetail;
use App\SubCategory;

class PageController extends Controller
{
    public function getIndex()
    {
        $slide = Slide::all()->take(8);
        //$productType = ProductType::all()->take(5);

        // lay 6 san pham dang khuyen mai
        $product_sale = Product::inRandomOrder()->where('promotion_price', '<>', 0)->take(6)->get();

        //lay 6 san pham trang diem
        $product_makeup = Product::inRandomOrder()->where('id_type', 1)->take(6)->get();

        //lay 6 san pham cham soc da
        $product_skincare = Product::inRandomOrder()->where('id_type', 2)->take(6)->get();

        //lay 6 san pham noi bat nhat random
        $product_top = Product::inRandomOrder()->where('top', 1)->take(6)->get();

        //lay 6 san pham nuoc hoa
        $product_perfume = Product::inRandomOrder()->where('id_type', 5)->take(6)->get();

        //lay 6 san pham phu kien
        $product_accessories = Product::inRandomOrder()->where('id_type', 6)->take(6)->get();
        //dd($product_sale);

    	return view('pages.trangchu', compact('slide', 'product_sale', 'product_makeup', 'product_top' , 'product_skincare', 'product_perfume', 'product_accessories'));
    }

    public function getChitiet($id, $id_type)
    {
        $product = Product::find($id);
        //dd($product);
        $product_type = TypeProduct::join('product', 'type_product.id', '=', 'product.id_type')->get();
        //dd($product_type);
        //$product_type = $product->type_product;

        $product_new = Product::where('id_type', '=', $id_type)->where('id', '<>', $id)->orderBy('id', 'DESC')->take(5)->get();

        $product_random = Product::all()->random(4);
    	return view('pages.chitiet', compact('product', 'product_type' ,'product_random', 'product_new'));
    }

    public function getLoaiSP($type, $id_type)
    {

        //phan trang 12 san pham tren mot trang
        $product_type = Product::where('id_type', $type)->paginate(12);
        //dd($product_type);
        $product_type_name = TypeProduct::where('id', $type)->first();
        //dd($product_type_name);
        
        
        $sub_category = SubCategory::where('id_sub', $product_type_name->id)->get();
        //dd($sub_category);

        $loaisp = TypeProduct::all();

    	return view('pages.loaisanpham', compact('product_type', 'product_type_name', 'loaisp', 'sub_category'));
    }

    public function getLoaiSPCon($id)
    {
        //phan trang 12 san pham tren mot trang
        $product_type = Product::where('id_subcategory', $id)->paginate(12);
        //dd($product_type);
        $product_type_name = TypeProduct::where('id', $id)->first();
        //dd($product_type_name);
        
        $sub_category = SubCategory::where('id_sub', $id)->get();
        //dd($sub_category);

        $loaisp = TypeProduct::all();

    	return view('pages.loaisanphamcon', compact('product_type', 'product_type_name', 'loaisp', 'sub_category'));
    }

    public function getTimKiem(Request $request)
    {
        $keyshow = $request->keyword;
        $loaisp = TypeProduct::all();
        $search_result = Product::where('name', 'like', '%'. $request->keyword . '%')->orWhere('unit_price', $request->keyword)->paginate(6);
        //dd($search_result);
        return view('pages.timkiem', compact('search_result', 'loaisp', 'keyshow'));
    }

    public function getTaiKhoan()
    {
        return view('pages.taikhoan');
    }

    public function getDangKy()
    {
        return view('pages.taikhoan');
    }

    public function postDangKy(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|max:60|unique:users,email',
            'hoten' => 'required|max:50',
            'diachi' => 'required|max:255',
            'sodienthoai' => 'required|max:11',
            'gioitinh' => 'required',
            'password' => 'required|max:20',
            'repassword' => 'required|max:20|same:password'
        ],
        [
            'email.required' => 'Địa chỉ email không được để trống. ',
            'email.max' => 'Email không vượt quá 60 kí tự. ',
            'email.unique' => 'Email đã có người sử dụng vui lòng kiểm tra lại. ',
            'hoten.required' => 'Họ tên không được để trống. ',
            'hoten.max' => 'Họ tên không vượt quá 60 kí tự. ',
            'diachi.required' => 'Địa chỉ không được để trống. ',
            'diachi.max' => 'Địa chỉ không vượt quá 60 kí tự. ',
            'sodienthoai.required' => 'Số điện thoại không được để trống. ',
            'sodienthoai.max' => 'Số điện thoại không được vượt quá 11 kí tự. ',
            'gioitinh.required' => 'Giới tính không được để trống. ',
            'password.required' => 'Mật khẩu không được để trống. ',
            'password.max' => 'Mật khẩu không vượt quá 20 kí tự. ',
            'repassword.required' => 'Trường nhập lại mật khẩu không được để trống. ',
            'repassword.max' => 'Trường nhập lại mật khẩu không vượt quá 20 kí tự. ',
            'repassword.same' => 'Mật khẩu không giống nhau. '
        ]);

        $user = new User();
        $user->name = $request->hoten;
        $user->email = $request->email;
        $user->gender = $request->gioitinh;
        $user->password = Hash::make($request->password);
        $user->phone = $request->sodienthoai;
        $user->address = $request->diachi;
        $user->save();

        // $customer = new Customer();
        // $customer->name = $request->hoten;
        // $customer->email = $request->email;
        // $customer->gender = $request->gioitinh;
        // $customer->phone_number = $request->sodienthoai;
        // $customer->address = $request->diachi;
        // $customer->save();

        return redirect()->back()->with(['flag' => 'success', 'thongbao' => 'Bạn đã đăng ký tài khoản thành công bây giờ bạn có thể đăng nhập!']);
    }

    public function getDangNhap(Request $request)
    {
        return view('page.dangnhap');
    }

    public function postDangNhap(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|max:60',
            'password' => 'required|max:20'
        ],
        [
            'email.required' => 'Địa chỉ email không được để trống',
            'email.max' => 'Địa chỉ email tối đa 60 kí tự',
            'password.required' => 'Mật khẩu không được để trống',
            'password.max' => 'Mật khẩu tối đa 20 kí tự'
        ]);

        $info = array('email' => $request->email, 'password' => $request->password);
        if(Auth::attempt($info)) {
            return redirect('/')->with(['flag' => 'success', 'message' => 'Đăng nhập thành công!']);
        } else {
            return redirect()->back()->with(['flag' => 'danger', 'message' => 'Đăng nhập không thành công vui lòng kiểm tra lại!']);
        }
    }

    public function getSanPhamKhuyenMai()
    {

        //phan trang 12 san pham tren mot trang
        $product_sale = Product::where('promotion_price', '<>', 0)->orderBy('id', 'DESC')->paginate(12);
        //dd($product_type);

        $loaisp = TypeProduct::all();

        return view('pages.sanphamkhuyenmai', compact('product_sale', 'loaisp'));
    }

    public function getGioiThieu()
    {
        return view('pages.gioithieu');
    }

    public function getHuongDanMuaHang()
    {
        return view('pages.huongdanmuahang');
    }

    public function getQuyDinhDoiTra()
    {
        return view('pages.quydinhdoitra');
    }

    public function getDangXuat()
    {
        Auth::logout();
        return view('pages.taikhoan');
    }

    //Cart
    public function getThemSanPham(Request $request, $id)
    {
        $product = Product::find($id);
        $oldcart = Session::has('cart') ? Session::get('cart') : null;
        //dd(Session::has('cart'));
        $cart = new Cart($oldcart);
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);
        //dd($request->session()->get('cart'));

    	return redirect()->back();
    }

    public function getMuaNgay(Request $request, $id)
    {
        $product = Product::find($id);
        $oldcart = Session::has('cart') ? Session::get('cart') : null;
        //dd(Session::has('cart'));
        $cart = new Cart($oldcart);
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);
        //dd($request->session()->get('cart'));

    	return redirect()->route('thanhtoan');
    }


    public function getXoaMotSanPham(Request $request, $id)
    {
        $oldcart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldcart);
        $cart->reduceByOne($id);

        Session::put('cart', $cart);
        // dd($request->session()->get('cart'));

    	return redirect()->back();
    }

    public function getXoaTatCaSP($id)
    {
        $oldcart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldcart);
        $cart->removeItem($id);

        Session::put('cart', $cart);

        return redirect()->back();
    }

    public function getGioHang(Request $request)
    {
        if(!Session::has('cart')) {
            return view('pages.thanhtoan', ['products' => null]);
        }
        $oldcart = Session::get('cart');
        $cart = new Cart($oldcart);
        //dd($cart->items, $cart->totalPrice,  $cart->totalPromotionPrice);
        return view('pages.giohang', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice, 'totalPromotionPrice' => $cart->totalPromotionPrice]);
    }


    public function getThanhtoan(Request $request)
    {
        if(!Session::has('cart')) {
            return view('pages.thanhtoan', ['products' => null]);
        }
        $oldcart = Session::get('cart');
        $cart = new Cart($oldcart);
        //dd($cart->items, $cart->totalPrice,  $cart->totalPromotionPrice);
        return view('pages.thanhtoan', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice, 'totalPromotionPrice' => $cart->totalPromotionPrice]);
    }

    public function postThanhToan(Request $request)
    {
        //dd($request);
        $this->validate($request, [
            'name' => 'required|max:255',
            'gender' => 'required|in:1,0',
            'email' => 'required|email',
            'address' => 'required|max:255',
            'phone' => 'max:13|required|min:10|digits_between:10,13'
        ],
        [
            'name.required' => 'Tên khách hàng không được để trống.',
            'name.max' => 'Tên khách hàng không vượt quá 255 ký tự.',
            'gender.required' => 'Giới tính không được để trống.',
            'email.required' => 'Địa chỉ email không được để trống.',
            'email.email' => 'Địa chỉ email không đúng định dạng.',
            'address.required' => 'Địa chỉ nhận hàng không được để trống.',
            'address.max' => 'Địa chỉ tối đa 255 ký tự',
            'phone.required' => 'Số điện thoại không được để trống.',
            'phone.digits_between' => 'Số điện thoại không đúng định dạng.',
            'phone.max' => 'Số điện thoại không quá 13 ký tự.',
            'phone.min' => 'Số điện thoại tối thiểu 10 ký tự'
        ]);
        $cart = Session::get('cart');
        //dd($cart);
        $customer = new Customer;
        $customer->name = $request->name;
        $customer->gender = $request->gender;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->phone_number = $request->phone;
        if($request->ghichu == '')
            $customer->note = "Không có ghi chú";
        else
            $customer->note = $request->ghichu;
        $customer->save();

        $bill = new Bill;
        $bill->id_customer = $customer->id;
        $bill->total = $request->price_payment;
        $bill->payment = $request->payment_method;
        if($request->ghichu == '')
            $bill->note = "Không có ghi chú";
        else
            $bill->note = $request->ghichu;
        $bill->save();

        foreach ($cart->items as $key => $value) {
            $billdetail = new BillDetail;
             $billdetail->id_bill = $bill->id;
             $billdetail->id_product = $key;
             $billdetail->quantity = $value['qty'];
             $billdetail->unit_price = $value['item']['unit_price'];
             $billdetail->promotion_price = $value['item']['promotion_price'] == 0 ? 0 : $value['item']['promotion_price'];
             $billdetail->save();
        }
 
        
        Session::forget('cart');
        return redirect()->back()->with('thongbao', 'Đặt hàng thành công. Nhân viên giao hàng sẽ liên hệ với bạn trong thời gian sớm nhất!');


    }

    //admin
    public function getLoginAdmin()
    {
        return view('admin/login');
    }

    public function getIndexAdmin()
    {
        return view('admin.index');
    }

    public function getSuaSanPham($id)
    {
        //phan trang 12 san pham tren mot trang
        $product_get = Product::where('id', $id)->first();

        $product_type_get_one = TypeProduct::where('id', $product_get->id_type)->first();

        $sub_category_get_one = SubCategory::where('id', $product_get->id_subcategory)->first();

        $product_type_get_all = TypeProduct::all();

        $sub_category_get_all = SubCategory::all();

        //dd($product_type_get);
        return view('admin.suasanpham', compact('product_get', 'product_type_get_one', 'product_type_get_all', 'sub_category_get_all', 'sub_category_get_one'));
    }

    public function getSanPham()
    {
        $product_type_name = TypeProduct::join('product', 'type_product.id', '=', 'product.id_type')->select('product.id AS id_product', 'product.name AS name_product', 'product.short_description AS short_description_product', 'product.long_description AS long_description_product', 
        'product.unit_price AS unit_price', 'product.promotion_price AS promotion_price', 'product.image AS image', 'product.top AS top', 'type_product.name AS name_type_product')->get();
        //dd($product_type_name);

        $type_product = TypeProduct::all();
        return view('admin.sanpham', compact('product_type_name', 'type_product'));
    }


    public function postLoginAdmin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|max:60',
            'password' => 'required|max:20'
        ],
        [
            'email.required' => 'Địa chỉ email không được để trống',
            'email.max' => 'Địa chỉ email tối đa 60 kí tự',
            'password.required' => 'Mật khẩu không được để trống',
            'password.max' => 'Mật khẩu tối đa 20 kí tự'
        ]);

        $info = array('email' => $request->email, 'password' => $request->password, 'role' => 1);
        if(Auth::attempt($info)) {
            return redirect('admin/index')->with(['flag' => 'success', 'message' => 'Đăng nhập thành công!', 'check' => "TRUE"]);
        } else {
            return redirect('admin/login')->with(['flag' => 'danger', 'message' => 'Đăng nhập không thành công vui lòng kiểm tra lại!']);
        }
    }

    public function postCapNhat(Request $request)
    {
        // /dd($request);
        if($request->has('submit'))
        {
            // dd($request);
            $name = 'fileupload';
            $name1 = 'fileupload1';
            $name2 = 'fileupload2';
            $name3 = 'fileupload3';
            // Thông báo khi xảy ra lỗi
            $messages = [
                
                'image' => 'Định dạng không cho phép',
                'max' => 'Kích thước file quá lớn',
            ];
            // Điều kiện cho phép upload
            $this->validate($request, [
                'fileupload' => 'image|max:10028',
            ], $messages);

            $flag1 = false;
            $flag2 = false;
            $flag3 = false;
            $flag4 = false;

            // Kiểm tra file hợp lệ
            if ($request->hasFile($name)){
                // Lấy tên file
                $file_name = $request->file($name)->getClientOriginalName();
                // Lưu file vào thư mục upload với tên là biến $filename
                $request->file($name)->move('source/uploads/product',$file_name);
                $flag1 = true;
            }

            if ($request->hasFile($name1)){
                // Lấy tên file
                $file_name1 = $request->file($name1)->getClientOriginalName();
                // Lưu file vào thư mục upload với tên là biến $filename
                $request->hasFile($name1)->move('source/uploads/product',$file_name1);
                $flag2 = true;
            }

            if ($request->hasFile($name2)){
                // Lấy tên file
                $file_name2 = $request->file($name2)->getClientOriginalName();
                // Lưu file vào thư mục upload với tên là biến $filename
                $request->file($name2)->move('source/uploads/product',$file_name2);
                $flag3 = true;
            }

            if ($request->hasFile($name3)){
                // Lấy tên file
                $file_name3 = $request->file($name3)->getClientOriginalName();
                // Lưu file vào thư mục upload với tên là biến $filename
                $request->file($name3)->move('source/uploads/product',$file_name3);
                $flag4 = true;
            }

            $product = new Product();
            $product->name = $request->tensp;
            $product->id_type = $request->loaisp;
            $product->id_subcategory = $request->danhmuccon;
            $product->short_description = $request->motangan;
            $product->long_description = $request->editor1;
            $giagoc = str_replace(".","",$request->giagoc);
            //$giagoc = str_replace("đ","",$giagoc);
            $product->unit_price = $giagoc;
            $giakm = str_replace(".","",$request->giakhuyenmai);
            //$giakm = str_replace("đ","",$giakm);
            $product->promotion_price = $giakm;
            if($flag1 == true)
                $product->image = $file_name;
            if($flag2 == true)
                $product->image1 = $file_name1;
            if($flag3 == true)
                $product->image2 = $file_name2;
            if($flag4 == true)
                $product->image3 = $file_name3;
            if($request->noibat == 'on')
                $product->top = 1;
            else
                $product->top = 0;
            $product->save();
            return redirect()->back()->with('thongbao', 'Cập nhật sản phẩm thành công!');

        }
    }

    public function getSlide()
    {
        if(Auth::check())
            redirect('admin.login');
        $slides = Slide::all();
        return view('admin/slide', compact('slides'));
    }

    public function postThemSlide(Request $request)
    {
        if($request->has('submitadd'))
        {
            $name = 'fileuploadadd';
            $this->validate($request, [
                'fileuploadadd' => 'required|image|max:10028'
            ], 
            [
                'fileuploadadd.required' => 'File ảnh không được để trống',
                'fileuploadadd.image' => 'Không phải định dạng ảnh',
                'fileuploadadd.max' => 'File ảnh tối đa 10M'
            ]);

            if ($request->file($name)->isValid()){
                // Lấy tên file
                $file_name = $request->file($name)->getClientOriginalName();
                // Lưu file vào thư mục upload với tên là biến $filename
                $request->file($name)->move('source/uploads/banner',$file_name);

                $slide = new Slide();
                $slide->link = 'source/image/product/banner' . $file_name;
                $slide->image = $file_name;
                $slide->save();

                return redirect('admin/slide')->with('thongbao', 'Thêm ảnh trình chiếu thành công!');
            }
        }
    }

    public function postXoaSlide(Request $request)
    {
        if($request->has('xoaslide'))
        {
            $slide = Slide::where('id', $request->idxoa)->delete();
            return redirect('admin/slide')->with('thongbao', 'Xóa ảnh trình chiếu ảnh thành công!');

        }
    }

    public function postCapNhatSlide(Request $request)
    {
        if($request->has('submit'))
        {
            $name = 'fileupload';
            $this->validate($request, [
                'fileupload' => 'required|image|max:10028'
            ], 
            [
                'fileupload.required' => 'File ảnh không được để trống',
                'fileupload.image' => 'Không phải định dạng ảnh',
                'fileupload.max' => 'File ảnh tối đa 10M'
            ]);

            if ($request->file($name)->isValid()){
                // Lấy tên file
                $file_name = $request->file($name)->getClientOriginalName();
                // Lưu file vào thư mục upload với tên là biến $filename
                $request->file($name)->move('source/image/banner',$file_name);

                //$slide = new Slide();
                $slide = Slide::find($request->idedit);
                $slide->link = 'source/image/product/banner' . $file_name;
                $slide->image = $file_name;
                $slide->save();

                return redirect('admin/slide')->with('thongbao', 'Cập nhật ảnh trình chiếu thành công!');
            }
            return redirect('admin/slide')->with('thongbao', 'Cập nhật trình chiếu ảnh không thành công!');

        }
    }











}
