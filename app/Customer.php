<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customer';

    public function bill()
    {
        return $this->hasOne('App\Bill', 'id', 'id_customer');
    }
}
