@extends('admin/master')
@section('contentcp')
@if(!Auth::check())
  <script>window.location = "admin/login";</script>
@endif
    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
          @if(Session::has('thongbao'))
            <div class="alert alert-success">{{ Session::get('thongbao') }}</div>
          @endif

          @if(count($errors) > 0)
            @foreach($errors->all() as $er)
                <div class="alert alert-danger">{{ $er }}</div>
            @endforeach
          @endif
        <h2>Danh sách đơn hàng chưa xử lý</h2>
      </div>
      <table id="sanpham" class="display table table-bordered table-hover">
       <thead>
          <tr>
            <th>ID KH</th>
             <th>Tên KH</th>
             <th>Email KH</th>
             <th>Sản phẩm</th>
             <th>Giá tiền 1 SP</th>
             <th>Được giảm</th>
             <th>Số lượng</th>
             <th>Tổng tiền</th>
             <th>Cần thanh toán</th>
             <th>Ngày đặt</th>
             <th>Ghi chú</th>
             <th>Thanh toán</th>
             <th>Thao tác</th>
          </tr>
       </thead>
       <tbody>

        @foreach($bill_details as $bill_detail)
          <tr>
            <th scope="row">{{ $bill_detail->id }}</th>
            <td>{{ $bill_detail->name }}</td>
            <td>{{ $bill_detail->email }}</td>
            <td><a href="{{ route('chitietsanpham', $bill_detail->id_product) }}">xem sản phẩm</a></td>
            <td>{{number_format(($bill_detail->unit_price), 0, ',', '.') }}đ</td>
            <td>{{number_format(($bill_detail->promotion_price == 0 ? 0 : $bill_detail->unit_price - $bill_detail->promotion_price), 0, ',', '.') }}đ</td>
             <td>{{ $bill_detail->quantity }}</td>
             <td>{{number_format(($bill_detail->unit_price * $bill_detail->quantity), 0, ',', '.') }}đ</td>
             <td>{{number_format(($bill_detail->unit_price - ($bill_detail->promotion_price == 0 ? 0 : $bill_detail->unit_price - $bill_detail->promotion_price)), 0, ',', '.') }}đ</td>
             <td>{{ $bill_detail->date_order }}</td>
             <td>{{ $bill_detail->note }}</td>
             <td>{{ $bill_detail->payment }}</td>
             <td><a class="dt-edit" style="cursor: pointer;">tiếp nhận đơn hàng</a></td>
          </tr>
          @endforeach

       </tbody>
      </table>

      <div class="page-header">
            @if(Session::has('thongbao'))
              <div class="alert alert-success">{{ Session::get('thongbao') }}</div>
            @endif
  
            @if(count($errors) > 0)
              @foreach($errors->all() as $er)
                  <div class="alert alert-danger">{{ $er }}</div>
              @endforeach
            @endif
          <h2>Danh sách đơn hàng đang xử lý</h2>
        </div>
        <table id="sanpham1" class="display table table-bordered table-hover">
         <thead>
            <tr>
              <th>ID KH</th>
               <th>Tên KH</th>
               <th>Email KH</th>
               <th>Sản phẩm</th>
               <th>Giá tiền 1 SP</th>
               <th>Được giảm</th>
               <th>Số lượng</th>
               <th>Tổng tiền</th>
               <th>Cần thanh toán</th>
               <th>Ngày đặt</th>
               <th>Ghi chú</th>
               <th>Thanh toán</th>
               <th>Thao tác</th>
            </tr>
         </thead>
         <tbody>
  
          @foreach($bill_details_proccess as $bill_detail)
            <tr>
              <th scope="row">{{ $bill_detail->id }}</th>
              <td>{{ $bill_detail->name }}</td>
              <td>{{ $bill_detail->email }}</td>
              <td><a href="{{ route('chitietsanpham', $bill_detail->id_product) }}">xem sản phẩm</a></td>
              <td>{{number_format(($bill_detail->unit_price), 0, ',', '.') }}đ</td>
              <td>{{number_format(($bill_detail->promotion_price == 0 ? 0 : $bill_detail->unit_price - $bill_detail->promotion_price), 0, ',', '.') }}đ</td>
               <td>{{ $bill_detail->quantity }}</td>
               <td>{{number_format(($bill_detail->unit_price * $bill_detail->quantity), 0, ',', '.') }}đ</td>
               <td>{{number_format(($bill_detail->unit_price - ($bill_detail->promotion_price == 0 ? 0 : $bill_detail->unit_price - $bill_detail->promotion_price)), 0, ',', '.') }}đ</td>
               <td>{{ $bill_detail->date_order }}</td>
               <td>{{ $bill_detail->note }}</td>
               <td>{{ $bill_detail->payment }}</td>
               <td><a class="dt-edit1" style="cursor: pointer;">đánh dấu đã xử lý</a></td>
            </tr>
            @endforeach
  
         </tbody>
        </table>
        <div class="page-header">
                @if(Session::has('thongbao'))
                  <div class="alert alert-success">{{ Session::get('thongbao') }}</div>
                @endif
      
                @if(count($errors) > 0)
                  @foreach($errors->all() as $er)
                      <div class="alert alert-danger">{{ $er }}</div>
                  @endforeach
                @endif
              <h2>Danh sách đơn hàng đã xử lý</h2>
            </div>
            <table id="sanpham2" class="display table table-bordered table-hover">
             <thead>
                <tr>
                  <th>ID KH</th>
                   <th>Tên KH</th>
                   <th>Email KH</th>
                   <th>Sản phẩm</th>
                   <th>Giá tiền 1 SP</th>
                   <th>Được giảm</th>
                   <th>Số lượng</th>
                   <th>Tổng tiền</th>
                   <th>Cần thanh toán</th>
                   <th>Ngày đặt</th>
                   <th>Ghi chú</th>
                   <th>Thanh toán</th>
                </tr>
             </thead>
             <tbody>
      
              @foreach($bill_details_success as $bill_detail)
                <tr>
                  <th scope="row">{{ $bill_detail->id }}</th>
                  <td>{{ $bill_detail->name }}</td>
                  <td>{{ $bill_detail->email }}</td>
                  <td><a href="{{ route('chitietsanpham', $bill_detail->id_product) }}">xem sản phẩm</a></td>
                  <td>{{number_format(($bill_detail->unit_price), 0, ',', '.') }}đ</td>
                  <td>{{number_format(($bill_detail->promotion_price == 0 ? 0 : $bill_detail->unit_price - $bill_detail->promotion_price), 0, ',', '.') }}đ</td>
                   <td>{{ $bill_detail->quantity }}</td>
                   <td>{{number_format(($bill_detail->unit_price * $bill_detail->quantity), 0, ',', '.') }}đ</td>
                   <td>{{number_format(($bill_detail->unit_price - ($bill_detail->promotion_price == 0 ? 0 : $bill_detail->unit_price - $bill_detail->promotion_price)), 0, ',', '.') }}đ</td>
                   <td>{{ $bill_detail->date_order }}</td>
                   <td>{{ $bill_detail->note }}</td>
                   <td>{{ $bill_detail->payment }}</td>
                </tr>
                @endforeach
      
             </tbody>
            </table>
          </div>
      </div>
    </div>
    

    <!-- Modal -->
    <div id="modalEdit" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="#"></form>
        <form action="{{ route('tiepnhandonhang') }}" method="POST" id="form" enctype="multipart/form-data" autocomplete="off">
          @csrf
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin đơn hàng</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <input type="hidden" class="form-control disabled" name="idedit" id="idedit">
              </div>
              <div class="form-group">
                <label for="exampleInputFile">Đánh dấu tiếp nhận đơn hàng này?</label>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-success" name="submit" value="Tiếp nhận đơn hàng">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    <!-- Modal -->
    <div id="modalEdit1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="#"></form>
        <form action="{{ route('xulydonhang') }}" method="POST" id="form" enctype="multipart/form-data" autocomplete="off">
        @csrf
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin đơn hàng</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <input type="hidden" class="form-control disabled" name="idedit1" id="idedit1">
            </div>
            <div class="form-group">
                <label for="exampleInputFile">Đánh dấu đã xử lý đơn hàng này?</label>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-success" name="submit" value="Đã xử lý">
        </div>
        <form>
        </div>

    </div>
    </div>
    <!-- End-Modal -->
      


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="sourceadmin/js/jquery-3.3.1.js"></script>
    <script src="sourceadmin/js/jquery.dataTables.min.js"></script>
    <script src="sourceadmin/js/dataTables.bootstrap.min.js"></script>
    <script src="sourceadmin/js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function() {
        $('#sanpham').DataTable({
          "pagingType": "full_numbers",
          "language": {
              "search": "Tìm kiếm:",
          }
        });
      });
    </script>

    <script>
        $(document).ready(function() {
          $('#sanpham1').DataTable({
            "pagingType": "full_numbers",
            "language": {
                "search": "Tìm kiếm:",
            }
          });
        });
    </script>
    <script>
            $(document).ready(function() {
              $('#sanpham2').DataTable({
                "pagingType": "full_numbers",
                "language": {
                    "search": "Tìm kiếm:",
                }
              });
            });
    </script>
    <script>
      //Edit row buttons
      $('.dt-edit').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idedit').val(dtRow[0].cells[0].innerHTML);
          }
          $('#modalEdit').modal('show');
        });
      });

       //Edit row buttons
       $('.dt-edit1').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idedit1').val(dtRow[0].cells[0].innerHTML);
          }
          $('#modalEdit1').modal('show');
        });
      });
    </script>
@endsection