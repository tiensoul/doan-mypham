@extends('admin/master')
@section('contentcp')
@if(!Auth::check())
  <script>window.location = "admin/login";</script>
@endif
    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
          @if(Session::has('thongbao'))
            <div class="alert alert-success">{{ Session::get('thongbao') }}</div>
          @endif

          @if(count($errors) > 0)
            @foreach($errors->all() as $er)
                <div class="alert alert-danger">{{ $er }}</div>
            @endforeach
          @endif
        <h2>Sửa chi tiết sản phẩm</h2>
      </div>

      <div class="row">
          <div class="col-md-4">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tên sản phẩm</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Tên của sản phẩm">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Mô tả ngắn về sản phẩm</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Mô tả ngắn">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Loại sản phẩm</label>
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Giá gốc của sản phẩm</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nhập giá gốc của sản phẩm">
                    </div>
                    <div class="form-group">
                            <label for="exampleInputPassword1">Giá khuyến mãi của sản phẩm</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nhập giá khuyễn mãi nếu có">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Hình ảnh sản phẩm hiện tại</label><br>
                        <img src="https://viblo.asia/uploads/93d02439-6f7e-4954-9c1e-76ac7038f804.png" width="80" height="80" class="img-rounded" alt="">
                        <img src="https://viblo.asia/uploads/93d02439-6f7e-4954-9c1e-76ac7038f804.png" width="80" height="80" class="img-rounded" alt="">
                        <img src="https://viblo.asia/uploads/93d02439-6f7e-4954-9c1e-76ac7038f804.png" width="80" height="80" class="img-rounded" alt="">
                        <img src="https://viblo.asia/uploads/93d02439-6f7e-4954-9c1e-76ac7038f804.png" width="80" height="80" class="img-rounded" alt="">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Tải lên hình ảnh cho sản phẩm</label>
                        <input type="file" id="exampleInputFile"><br>
                        <input type="file" id="exampleInputFile"><br>
                        <input type="file" id="exampleInputFile"><br>
                        <input type="file" id="exampleInputFile"><br>
                        <p class="help-block">Tải lên ít nhất một hình ảnh cho sản phẩm.</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Tích vào ô để đặt sản phẩm là nổi bật</label>
                        <br>
                        <input type="checkbox"> Nổi bật
                    </div>
                    <button type="submit" class="btn btn-default">Cập nhật sản phẩm</button>
                </form>
          </div>
          <div class="col-md-8">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mô tả chi tiết sản phẩm</label>
                        <textarea name="editor1" id="editor1" rows="10" cols="80">
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        <input type="file" id="exampleInputFile">
                        <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <div class="checkbox">
                        <label>
                        <input type="checkbox"> Check me out
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
          </div>
      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="sourceadmin/js/jquery-3.3.1.js"></script>
    <script src="sourceadmin/js/jquery.dataTables.min.js"></script>
    <script src="sourceadmin/js/dataTables.bootstrap.min.js"></script>
    <script src="sourceadmin/js/bootstrap.min.js"></script>
@endsection