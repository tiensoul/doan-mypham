@extends('pages/master')
@section('content')
<script src="js/jssor.slider-27.5.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideoTransitions = [
              [{b:-1,d:1,o:-0.7}],
              [{b:900,d:2000,x:-379,e:{x:7}}],
              [{b:900,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,sX:2,sY:2},{b:0,d:900,x:-171,y:-341,o:1,sX:-2,sY:-2,e:{x:3,y:3,sX:3,sY:3}},{b:900,d:1600,x:-283,o:-1,e:{x:16}}]
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /*jssor slider loading skin spin css*/
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }

        /*jssor slider bullet skin 032 css*/
        .jssorb032 {position:absolute;}
        .jssorb032 .i {position:absolute;cursor:pointer;}
        .jssorb032 .i .b {fill:#fff;fill-opacity:0.7;stroke:#000;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.25;}
        .jssorb032 .i:hover .b {fill:#000;fill-opacity:.6;stroke:#fff;stroke-opacity:.35;}
        .jssorb032 .iav .b {fill:#000;fill-opacity:1;stroke:#fff;stroke-opacity:.35;}
        .jssorb032 .i.idn {opacity:.3;}

        /*jssor slider arrow skin 051 css*/
        .jssora051 {display:block;position:absolute;cursor:pointer;}
        .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
        .jssora051:hover {opacity:.8;}
        .jssora051.jssora051dn {opacity:.5;}
        .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
    </style>
<!-- MAIN CONTENT AREA -->
<div class="container">
<div class="row">
        <div class="site-content nuty-tab-product-cat" role="main" style="display: none;">
            <div class="list-cat-wrapper">

            </div>
        </div>
        <div class="site-content nuty-tab-home" role="main">
            <!---------HOME SLIDER SECTION---------->
            <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height: 360px;overflow:hidden;visibility:hidden;">
                    <!-- Loading Screen -->
                    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
                    </div>
                    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:380px;overflow:hidden;">
                        @foreach($slide as $sl)
                        <div>
                            <img data-u="image" src="{{ $sl->link }}" style="display: block;
                            object-fit: cover;" />
                        </div>
                        @endforeach
                        
                    </div>
                    <!-- Bullet Navigator -->
                    <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                        <div data-u="prototype" class="i" style="width:16px;height:16px;">
                            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                            </svg>
                        </div>
                    </div>
                    <!-- Arrow Navigator -->
                    <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                        </svg>
                    </div>
                    <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                        </svg>
                    </div>
                </div>
                <script type="text/javascript">jssor_1_slider_init();</script>
                <!-- #endregion Jssor Slider End -->
        </div>
            <!---------SALE PRDOUCT SECTION---------->
            <div class="suntory-sale-products">
                <div class="container">
                    <div class="row">
                        <div class="hometitle-wrapper" style="margin-bottom: 30px;">
                            <h3 class="home-title2">Sản phẩm khuyến mãi</h3>
                        </div>

                        <!--MAC DINH SAN PHAM SALE--->
                        <!----->
                        <div class="basel-products-element">
                            <div class="basel-products-loader"></div>
                            <div class="products elements-grid row basel-products-holder  pagination-arrows grid-columns-6">

                                @foreach($product_sale as $ps)
                                <div class="product-grid-item basel-hover-alt product  col-xs-6 col-sm-4 col-md-2 first  post-107506 type-product status-publish has-post-thumbnail product_cat-xit-khoa-nen product_tag-byphasse product_tag-byphasse-fix-make-up-long-lasting product_tag-sale2004 product_tag-salesn product_tag-xit-khoa-lop-trang-diem product_tag-xit-khoa-nen first instock shipping-taxable purchasable product-type-simple"
                                    data-loop="1" data-id="107506">

                                    <div class="product-element-top">
                                        @if($ps->promotion_price != 0)
                                            <div class="product-labels labels-rectangular"><span class="onsale product-label">-{{ number_format((100 - ($ps->promotion_price / $ps->unit_price) * 100), 0) }}%</span></div>
                                        @endif
                                        <a href="{{ route('chitiet', [$ps->id, $ps->id_type]) }}">
                                            <img width="300" height="300"
                                        src="uploads/product/{{ $ps->image }}"
                                                class="lazy lazy-hidden jetpack-lazy-image" alt="" />
                                        </a>
                                        <div class="basel-buttons">
                                        </div>
                                    </div>
                                    <h3 class="product-title"><a
                                    href="{{ route('chitiet', [$ps->id, $ps->id_type]) }}">{{ $ps->name }}</a></h3>

                                    <div class="wrap-price">
                                        <div class="wrapp-swap">
                                            <div class="swap-elements">

                                                    <span class="price"><span class="woocommerce-Price-amount amount">
                                                            @if($ps->promotion_price != 0)
                                                            <span style="text-decoration: line-through; color: #a52a2a;">
                                                                {{ number_format(($ps->unit_price), 0, '.', ',') }}đ</span> - {{ number_format(($ps->promotion_price), 0, '.', ',') }}đ
                                                            @else
                                                                {{ number_format(($ps->unit_price), 0, '.', ',') }}đ
                                                            @endif
                                                            </span>
                                                        </span>
                                                <div class="btn-add">
                                                    <a href="{{ route('cartAdd', $ps->id) }}"
                                                        class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm vào giỏ</a> </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                @endforeach
                                <div class="clearfix visible-xs-block"></div>
                                <div class="clearfix visible-sm-block"></div>
                                <div class="clearfix visible-md-block visible-lg-block"></div>
                            </div>
                            <div class="products-footer">
                            </div>
                        </div>
                        <div class="basel-products-element">
                            <div class="basel-products-loader"></div>
                            <div class="products elements-grid row basel-products-holder  pagination-arrows grid-columns-6"
                                data-paged="1" data-source="shortcode"
                                data-atts="{&quot;post_type&quot;:&quot;product&quot;,&quot;layout&quot;:&quot;grid&quot;,&quot;include&quot;:&quot;&quot;,&quot;custom_query&quot;:&quot;&quot;,&quot;taxonomies&quot;:&quot;11759&quot;,&quot;pagination&quot;:&quot;arrows&quot;,&quot;items_per_page&quot;:&quot;6&quot;,&quot;product_hover&quot;:&quot;alt&quot;,&quot;columns&quot;:&quot;6&quot;,&quot;sale_countdown&quot;:0,&quot;offset&quot;:&quot;&quot;,&quot;orderby&quot;:&quot;date&quot;,&quot;query_type&quot;:&quot;OR&quot;,&quot;order&quot;:&quot;DESC&quot;,&quot;meta_key&quot;:&quot;&quot;,&quot;exclude&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;ajax_page&quot;:&quot;&quot;,&quot;speed&quot;:&quot;5000&quot;,&quot;slides_per_view&quot;:&quot;1&quot;,&quot;wrap&quot;:&quot;&quot;,&quot;autoplay&quot;:&quot;no&quot;,&quot;hide_pagination_control&quot;:&quot;yes&quot;,&quot;hide_prev_next_buttons&quot;:&quot;&quot;,&quot;scroll_per_page&quot;:&quot;yes&quot;,&quot;carousel_js_inline&quot;:&quot;no&quot;,&quot;img_size&quot;:&quot;woocommerce_thumbnail&quot;,&quot;force_not_ajax&quot;:&quot;no&quot;,&quot;center_mode&quot;:&quot;no&quot;,&quot;products_masonry&quot;:&quot;&quot;,&quot;products_different_sizes&quot;:&quot;&quot;}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="hometitle-wrapper" style="margin-bottom: 30px;">
                                <h3 class="home-title2">Sản phẩm nổi bật</h3>
                            </div>
    
                            <!--MAC DINH SAN PHAM SALE--->
                            <!----->
                            <div class="basel-products-element">
                                <div class="basel-products-loader"></div>
                                <div class="products elements-grid row basel-products-holder  pagination-arrows grid-columns-6">
    
                                    @foreach($product_top as $ps)
                                    <div class="product-grid-item basel-hover-alt product  col-xs-6 col-sm-4 col-md-2 first  post-107506 type-product status-publish has-post-thumbnail product_cat-xit-khoa-nen product_tag-byphasse product_tag-byphasse-fix-make-up-long-lasting product_tag-sale2004 product_tag-salesn product_tag-xit-khoa-lop-trang-diem product_tag-xit-khoa-nen first instock shipping-taxable purchasable product-type-simple"
                                        data-loop="1" data-id="107506">
    
                                        <div class="product-element-top">
                                            @if($ps->promotion_price != 0)
                                                <div class="product-labels labels-rectangular"><span class="onsale product-label">-{{ number_format((100 - ($ps->promotion_price / $ps->unit_price) * 100), 0) }}%</span></div>
                                            @endif
                                            <a href="{{ route('chitiet', [$ps->id, $ps->id_type]) }}">
                                                <img width="300" height="300"
                                            src="uploads/product/{{ $ps->image }}"
                                                    class="lazy lazy-hidden jetpack-lazy-image" alt="" />
                                            </a>
                                            <div class="basel-buttons">
                                            </div>
                                        </div>
                                        <h3 class="product-title"><a
                                        href="{{ route('chitiet', [$ps->id, $ps->id_type]) }}">{{ $ps->name }}</a></h3>
    
                                        <div class="wrap-price">
                                            <div class="wrapp-swap">
                                                <div class="swap-elements">
    
                                                    <span class="price"><span class="woocommerce-Price-amount amount">
                                                            @if($ps->promotion_price != 0)
                                                            <span style="text-decoration: line-through; color: #a52a2a;">
                                                                {{ number_format(($ps->unit_price), 0, '.', ',') }}đ</span> - {{ number_format(($ps->promotion_price), 0, '.', ',') }}đ
                                                            @else
                                                                {{ number_format(($ps->unit_price), 0, '.', ',') }}đ
                                                            @endif
                                                            </span>
                                                        </span>
                                                    <div class="btn-add">
                                                        <a href="{{ route('cartAdd', $ps->id) }}"
                                                            class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm vào giỏ</a> </div>
                                                </div>
                                            </div>
    
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="clearfix visible-xs-block"></div>
                                    <div class="clearfix visible-sm-block"></div>
                                    <div class="clearfix visible-md-block visible-lg-block"></div>
                                </div>
                                <div class="products-footer">
                                </div>
                            </div>
                            <div class="basel-products-element">
                                <div class="basel-products-loader"></div>
                                <div class="products elements-grid row basel-products-holder  pagination-arrows grid-columns-6"
                                    data-paged="1" data-source="shortcode"
                                    data-atts="{&quot;post_type&quot;:&quot;product&quot;,&quot;layout&quot;:&quot;grid&quot;,&quot;include&quot;:&quot;&quot;,&quot;custom_query&quot;:&quot;&quot;,&quot;taxonomies&quot;:&quot;11759&quot;,&quot;pagination&quot;:&quot;arrows&quot;,&quot;items_per_page&quot;:&quot;6&quot;,&quot;product_hover&quot;:&quot;alt&quot;,&quot;columns&quot;:&quot;6&quot;,&quot;sale_countdown&quot;:0,&quot;offset&quot;:&quot;&quot;,&quot;orderby&quot;:&quot;date&quot;,&quot;query_type&quot;:&quot;OR&quot;,&quot;order&quot;:&quot;DESC&quot;,&quot;meta_key&quot;:&quot;&quot;,&quot;exclude&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;ajax_page&quot;:&quot;&quot;,&quot;speed&quot;:&quot;5000&quot;,&quot;slides_per_view&quot;:&quot;1&quot;,&quot;wrap&quot;:&quot;&quot;,&quot;autoplay&quot;:&quot;no&quot;,&quot;hide_pagination_control&quot;:&quot;yes&quot;,&quot;hide_prev_next_buttons&quot;:&quot;&quot;,&quot;scroll_per_page&quot;:&quot;yes&quot;,&quot;carousel_js_inline&quot;:&quot;no&quot;,&quot;img_size&quot;:&quot;woocommerce_thumbnail&quot;,&quot;force_not_ajax&quot;:&quot;no&quot;,&quot;center_mode&quot;:&quot;no&quot;,&quot;products_masonry&quot;:&quot;&quot;,&quot;products_different_sizes&quot;:&quot;&quot;}">
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!---------RECENT PRDOUCT SECTION---------->
            <div class="suntory-recent-products">
                <div class="container">

                    <div class="row">
                        <div class="hometitle-wrapper">
                            <h3 class="home-title2">Trang điểm</h3>
                        </div>
                        <div id="carousel-265" class="vc_carousel_container " data-owl-carousel
                            data-hide_pagination_control="yes" data-desktop="6" data-desktop_small="4" data-tablet="4"
                            data-mobile="2">
                            <div class="owl-carousel product-items ">

                                @foreach($product_makeup as $pm)
                                <div class="product-item owl-carousel-item">
                                    <div class="owl-carousel-item-inner">
                                        <div class="product-grid-item basel-hover-alt product product-in-carousel post-140115 type-product status-publish has-post-thumbnail product_cat-trang-diem product_cat-eyes-makeup product_cat-phan-mat first instock shipping-taxable purchasable product-type-simple"
                                            data-loop="1" data-id="140115">

                                            <div class="product-element-top">
                                                @if($pm->promotion_price != 0)
                                                    <div class="product-labels labels-rectangular"><span class="onsale product-label">-{{ number_format((100 - ($pm->promotion_price / $pm->unit_price) * 100), 0) }}%</span></div>
                                                @endif
                                                <a
                                                    href="{{ route('chitiet', [$pm->id, $pm->id_type]) }}">
                                                    <img width="300" height="300" src="uploads/product/{{ $pm->image }}"
                                                        class="lazy lazy-hidden jetpack-lazy-image" alt="" />
                                                </a>
                                                <div class="basel-buttons">
                                                </div>
                                            </div>
                                            <h3 class="product-title"><a
                                            href="{{ route('chitiet', [$pm->id, $pm->id_type]) }}">{{ $pm->name }}</a></h3>

                                            <div class="wrap-price">
                                                <div class="wrapp-swap">
                                                    <div class="swap-elements">

                                                        <span class="price"><span class="woocommerce-Price-amount amount">
                                                            @if($pm->promotion_price != 0)
                                                            <span style="text-decoration: line-through; color: #a52a2a;">
                                                                {{ number_format(($pm->unit_price), 0, '.', ',') }}đ</span> - {{ number_format(($pm->promotion_price), 0, '.', ',') }}đ
                                                            @else
                                                                {{ number_format(($pm->unit_price), 0, '.', ',') }}đ
                                                            @endif
                                                            </span>
                                                        </span>
                                                        <div class="btn-add">
                                                            <a href="{{ route('cartAdd', $pm->id) }}" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm vào giỏ</a> </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div> <!-- end product-items -->
                        </div> <!-- end #carousel-265 -->

                    </div>

                    <div class="row">
                        <div class="hometitle-wrapper" style="margin-top: 20px;">
                            <h3 class="home-title2">Chăm sóc da</h3>
                        </div>
                        <div id="carousel-531" class="vc_carousel_container " data-owl-carousel
                            data-hide_pagination_control="yes" data-desktop="6" data-desktop_small="4" data-tablet="4"
                            data-mobile="2">
                            <div class="owl-carousel product-items ">

                                @foreach($product_skincare as $psk)
                                <div class="product-item owl-carousel-item">
                                    <div class="owl-carousel-item-inner">

                                        <div
                                            class="product-grid-item basel-hover-alt product product-in-carousel post-140065 type-product status-publish has-post-thumbnail product_cat-tay-te-bao-chet first instock shipping-taxable purchasable product-type-simple"
                                            data-loop="1" data-id="140065">

                                            <div class="product-element-top">
                                                @if($psk->promotion_price != 0)
                                                    <div class="product-labels labels-rectangular"><span class="onsale product-label">-{{ number_format((100 - ($pm->promotion_price / $pm->unit_price) * 100), 0) }}%</span></div>
                                                @endif
                                                <a
                                                    href="{{ route('chitiet', [$psk->id, $psk->id_type]) }}">
                                                    <img width="300" height="300" src="uploads/product/{{ $psk->image }}" class="lazy lazy-hidden jetpack-lazy-image" alt="" data-lazy-type="image" />
                                                </a>
                                                <div class="basel-buttons">
                                                </div>
                                            </div>
                                            <h3 class="product-title"><a
                                                    href="{{ route('chitiet', [$psk->id, $psk->id_type]) }}">{{ $psk->name }}</a></h3>

                                            <div class="wrap-price">
                                                <div class="wrapp-swap">
                                                    <div class="swap-elements">

                                                        <span class="price"><span class="woocommerce-Price-amount amount">
                                                                @if($psk->promotion_price != 0)
                                                                <span style="text-decoration: line-through; color: #a52a2a;">
                                                                    {{ number_format(($psk->unit_price), 0, '.', ',') }}đ</span> - {{ number_format(($psk->promotion_price), 0, '.', ',') }}đ
                                                                @else
                                                                    {{ number_format(($psk->unit_price), 0, '.', ',') }}đ
                                                                @endif
                                                                </span>
                                                        </span>
                                                        <div class="btn-add">
                                                            <a href="{{ route('cartAdd', $psk->id) }}" data-quantity="1"
                                                                class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm vào giỏ</a> </div>
                                                    </div>
                                                </div>

                                            </div>



                                        </div>

                                    </div>
                                </div>
                                @endforeach

                            </div> <!-- end product-items -->
                        </div> <!-- end #carousel-531 -->

                    </div>

                    <div class="row">
                        <div class="hometitle-wrapper" style="margin-top: 20px;">
                            <h3 class="home-title2">Nước hoa</h3>
                        </div>
                        <div id="carousel-783" class="vc_carousel_container " data-owl-carousel
                            data-hide_pagination_control="yes" data-desktop="6" data-desktop_small="4" data-tablet="4"
                            data-mobile="2">
                            <div class="owl-carousel product-items ">

                                @foreach($product_perfume as $pp)
                                <div class="product-item owl-carousel-item">
                                    <div class="owl-carousel-item-inner">

                                        <div
                                            class="product-grid-item basel-hover-alt product product-in-carousel post-140122 type-product status-publish has-post-thumbnail product_cat-cham-soc-da product_cat-kem-duong-da first instock shipping-taxable purchasable product-type-variable has-default-attributes"
                                            data-loop="1" data-id="140122">

                                            <div class="product-element-top">
                                                @if($pp->promotion_price != 0)
                                                    <div class="product-labels labels-rectangular"><span class="onsale product-label">-{{ number_format((100 - ($pp->promotion_price / $pp->unit_price) * 100), 0) }}%</span></div>
                                                @endif
                                                <a href="{{ route('chitiet', [$pp->id, $pp->id_type]) }}">
                                                    <img width="300" height="300" src="uploads/product/{{ $pp->image }}" alt="" />
                                                </a>
                                                <div class="basel-buttons">
                                                </div>
                                            </div>
                                            <h3 class="product-title"><a
                                                    href="{{ route('chitiet', [$pp->id, $pp->id_type]) }}">{{ $pp->name }}</a></h3>

                                            <div class="wrap-price">
                                                <div class="wrapp-swap">
                                                    <div class="swap-elements">

                                                        <span class="price"><span class="woocommerce-Price-amount amount">
                                                                @if($pp->promotion_price != 0)
                                                                <span style="text-decoration: line-through; color: #a52a2a;">
                                                                    {{ number_format(($pp->unit_price), 0, '.', ',') }}đ</span> - {{ number_format(($pp->promotion_price), 0, '.', ',') }}đ
                                                                @else
                                                                    {{ number_format(($pp->unit_price), 0, '.', ',') }}đ
                                                                @endif
                                                                </span>
                                                        </span>
                                                        <div class="btn-add">
                                                            <a href="{{ route('cartAdd', $pp->id) }}" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm vào giỏ</a> </div>
                                                    </div>
                                                </div>

                                            </div>



                                        </div>

                                    </div>
                                </div>
                                @endforeach
                            </div> <!-- end product-items -->
                        </div> <!-- end #carousel-783 -->

                    </div>

                    <div class="row">
                        <div class="hometitle-wrapper" style="margin-top: 20px;">
                            <h3 class="home-title2">Phụ kiện</h3>
                        </div>
                        <div id="carousel-110" class="vc_carousel_container " data-owl-carousel
                            data-hide_pagination_control="yes" data-desktop="6" data-desktop_small="4" data-tablet="4"
                            data-mobile="2">
                            <div class="owl-carousel product-items ">

                                @foreach($product_accessories as $pa)
                                <div class="product-item owl-carousel-item">
                                    <div class="owl-carousel-item-inner">

                                        <div
                                            class="product-grid-item basel-hover-alt product product-in-carousel post-139236 type-product status-publish has-post-thumbnail product_cat-nuoc-hoa product_cat-nuoc-hoa-nu first instock shipping-taxable purchasable product-type-simple"
                                            data-loop="1" data-id="139236">

                                            <div class="product-element-top">
                                                @if($pa->promotion_price != 0)
                                                    <div class="product-labels labels-rectangular"><span class="onsale product-label">-{{ number_format((100 - ($pa->promotion_price / $pa->unit_price) * 100), 0) }}%</span></div>
                                                @endif
                                                <a href="{{ route('chitiet', [$pa->id, $pa->id_type]) }}">
                                                    <img width="300" height="300"
                                                        src="uploads/product/{{ $pa->image }}"
                                                        class="lazy lazy-hidden jetpack-lazy-image" alt="" />
                                                </a>
                                                <div class="basel-buttons">
                                                </div>
                                            </div>
                                            <h3 class="product-title"><a
                                                    href="{{ route('chitiet', [$pa->id, $pa->id_type]) }}">{{ $pa->name }}</a></h3>

                                            <div class="wrap-price">
                                                <div class="wrapp-swap">
                                                    <div class="swap-elements">

                                                            <span class="price"><span class="woocommerce-Price-amount amount">
                                                                    @if($pa->promotion_price != 0)
                                                                    <span style="text-decoration: line-through; color: #a52a2a;">
                                                                        {{ number_format(($pa->unit_price), 0, '.', ',') }}đ</span> - {{ number_format(($pa->promotion_price), 0, '.', ',') }}đ
                                                                    @else
                                                                        {{ number_format(($pa->unit_price), 0, '.', ',') }}đ
                                                                    @endif
                                                                    </span>
                                                            </span>
                                                        <div class="btn-add">
                                                            <a href="{{ route('cartAdd', $pa->id) }}"
                                                                class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm vào giỏ</a> </div>
                                                    </div>
                                                </div>

                                            </div>



                                        </div>

                                    </div>
                                </div>
                                @endforeach
                            </div> <!-- end product-items -->
                        </div> <!-- end #carousel-110 -->

                    </div>

                </div>
            </div>
        </div><!-- .site-content -->

        <style type="text/css">
            @media (min-width: 1200px) {
                .chinhanh-item {
                    width: 20%;
                    padding: 0 5px 0 5px;
                }
            }
        </style>


</div> <!-- end row -->
</div> <!-- end container -->
@endsection('content')