@extends('pages/master')
@section('content')
</div>
<div class="container" style="margin-top: 40px;>
    <div class="row">




        <div class="site-content col-sm-12 col-sm-push-0" role="main">

            <article id="post-22305" class="post-22305 page type-page status-publish">

                <div class="entry-content">
                    <h3>Cách thức mua hàng</h3>
                    <p>Mua hàng tại các Chi nhánh của Nuty tại Thành phố Hồ Chí Minh:</p>
                    <ul>
                        <li style="list-style-type: none;">
                            <ul>
                                <li>Add 1: 31/7 Hoàng Việt, P.4, Q. Tân Bình (sỉ)</li>
                                <li>Add 2: 490 đường 3/2, P.14,quận 10 (lẻ)</li>
                                <li>Add 3: 235 Khánh Hội, P.5, Quận 4 (lẻ)</li>
                                <li>Add 4: 19 Út Tịch (22 Hoàng Việt), P.4 Q.TB (lẻ)</li>
                                <li>Add 5: 455 – 457 Phan Văn Trị, P.5, Q. Gò Vấp (lẻ)</li>
                            </ul>
                        </li>
                    </ul>
                    <p>Mở cửa từ 9h-22h (từ thứ 2 đến chủ nhật)</p>
                    <p>Đặt hàng Online qua 1 trong 4 hình thức sau:</p>
                    <ol>
                        <li>Hotline của Nuty Cosmetics: 1900 636 737 (Nhấn phím 1)</li>
                        <li>Liên hệ trực tiếp số điện thoại bàn &lrm;0886 838 939 (lẻ) hoặc &lrm;0948 685 843 (sỉ)</li>
                        <li>Mua hàng tại Website: www.nuty.vn – www.nutycosmetics.vn</li>
                        <li>Đặt hàng qua Facebook: www.facebook.com/nutycosmetics.</li>
                    </ol>
                    <p>Tổng đài CSKH: &lrm;1900 636 737 (phím số &lrm;8)</p>
                    <p>Điện thoại CSKH: 0287 3000 333</p>
                    <p>Email: cskh.nutycosmetics@gmail.com</p>
                    <hr>
                    <h3>Cách thức Thanh toán:</h3>
                    <ol>
                        <li><strong>Đối với các bạn ở Thành phố Hồ Chí Minh:</strong>+ Thanh toán trực tiếp tại điểm mua
                            hàng: tiền mặt hoặc thanh toán bằng thẻ.+ Giao hàng và thanh toán tiền tại nhà: <a
                                href="https://www.facebook.com/notes/nuty-cosmetics/b%E1%BA%A3ng-gi%C3%A1-ship-khu-v%E1%BB%B1c-h%E1%BB%93-ch%C3%AD-minh/816609175018691">Phí
                                Ship tại Tp. HCM</a></li>
                        <li><strong>Khách ở các tỉnh thành trên cả nước:</strong>Sau khi chốt đơn hàng bạn chuyển khoản
                            100% tiền hàng + tiền ship(nếu có) vào tài khoản của shop.</li>
                        <li><strong>Lưu ý: Qúy khách phải đợi xác nhận đơn hàng qua điện thoại từ nhân viên Nuty rồi mới
                                thực &nbsp;hiện chuyển khoản&nbsp;</strong>Hình thức giao hàng: Chuyển phát nhanh tận
                            nhà hoặc gửi các nhà xe… tùy bạn chọn. Thời gian nhận hàng sẽ từ 1-5 ngày(tùy địa chỉ, khu
                            vực) nếu chuyển nhanh hoặc nhà xe, gửi chuyển phát chậm sẽ mất từ 3-7 ngày tùy khu vực.Phí
                            giao hàng: Sẽ phụ thuộc vào khối lượng và kích thước của gói hàng và hình thức giao hàng bạn
                            chọn:Gửi nhà xe: phí gửi xe Tô Châu, Phương Trang, Việt Tân Phát, Hoàng Long… shop sẽ thu
                            đúng theo quy định của mỗi nhà xe. Gửi công ty chuyển phát nhanh: Hàng sẽ đến tận nhà bạn và
                            bạn sẽ tự trả ship sau khi nhận được hàng.Shop luôn mong muốn chọn được dịch vụ giao hàng
                            thuận tiện và tiết kiệm nhất cho khách hàng, nếu có bất kì góp ý và phàn nàn nào về dịch vụ
                            bạn có thể liên hệ ngay với shop số hotline 1900 636 737 – Nhấn phím 8.</li>
                    </ol>
                    <hr>
                    <h3>Thông tin tài khoản:</h3>
                    <ol>
                        <li>NGÂN HÀNG VIETCOMBANK
                            <ul>
                                <li>Chủ Tài Khoản: Nguyễn Thị Thảo Nguyên</li>
                                <li>Số Tk: 0251002759580</li>
                                <li>Chi nhánh: Vietcombank Chi Nhánh Bình Tây</li>
                            </ul>
                        </li>
                        <li>NGÂN HÀNG ĐÔNG Á
                            <ul>
                                <li>Chủ Tài Khoản: Nguyễn Đức Thắng</li>
                                <li>Số Tk: 0105208173</li>
                                <li>Chi nhánh: TP.HCM</li>
                            </ul>
                        </li>
                    </ol>
                </div>


            </article><!-- #post -->



        </div><!-- .site-content -->



    </div> <!-- end row -->
</div>
@endsection('content')