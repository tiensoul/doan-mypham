<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TypeProductSeeder::class,
            SubCategorySeeder::class,
            ProductSeeder::class,
            SlideSeeder::class,
            UserSeeder::class
        ]);
    }
}
